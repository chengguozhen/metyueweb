package metyue.service;

import java.util.List;
import java.util.Map;

import metyue.dao.MemberCenterDao;
import metyue.model.MemberCenterVO;
import metyue.model.UserFans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MemberCenterService {

	@Autowired
	private MemberCenterDao memberCenterDao;
	
	public void trunOnFans(long userPtr, long fansPtr){
		
		memberCenterDao.trunOnFans(userPtr, fansPtr);
	}
	
	public List<MemberCenterVO> getMemberCenterList(Map<String, String> para){
		
		return memberCenterDao.getUserPhotoInfoVOList(para);
	}
	
	public List<MemberCenterVO> getUserPhotoInfoVOByUserPtr(Map<String, String> para){
		
		return memberCenterDao.getUserPhotoInfoVOByUserPtr(para);
	}
}
