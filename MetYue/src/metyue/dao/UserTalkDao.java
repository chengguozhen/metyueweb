package metyue.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import metyue.model.UserTalkInfo;
import metyue.model.UserTalkInfoVO;
import metyue.model.UserTalkPanel;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class UserTalkDao {

	@Autowired
    private HibernateTemplate hibernateTemplate;
	
	//发送消息
	public void send(UserTalkInfo userTalkInfo){
		
		hibernateTemplate.saveOrUpdate(userTalkInfo);
	}
	
	
	//未读信息的条数
	public int getNotReadTalkInfoCount(long receiveUserPtr){		
		
		int result = 0;
		
		try{
			
			final StringBuilder queryStr = new StringBuilder();

			queryStr.append(" SELECT COUNT(PKEY) AS NOT_TALK_COUNT FROM USER_TALK_INFO WHERE RECEIVE_USER_PTR=" + receiveUserPtr + " AND IS_READ_FLG='1' ");
			
			result = (int) this.hibernateTemplate.execute(new HibernateCallback<Object>() {
		    	
		    	public Object doInHibernate(final Session session)throws HibernateException, SQLException {
		    		
		    		Query query = session.createSQLQuery(queryStr.toString());
		    		
		    		return Integer.parseInt(query.uniqueResult().toString());
		    	}
		    	
		    });
    
		} catch (Exception e) {
			
			e.printStackTrace();
	   	}
		
		return result;
		
	}
	
	//获取信息面板
	@SuppressWarnings("unchecked")
	public List<UserTalkPanel> getUserTalkPanelList(long receiveUserPtr){
		
		List<UserTalkPanel> list = new ArrayList<UserTalkPanel>();
		
		try {
			
		    String queryStr = " FROM UserTalkPanel a WHERE a.receiveUserPtr = ? order by a.count desc";
		    
		    list = (List<UserTalkPanel>)hibernateTemplate.find(queryStr, receiveUserPtr);

		} catch (Exception e) {
			
			e.printStackTrace();
	   	}
		
		return list;
	}
	
	//更新已读
	public int resetReadded(long sendUserPtr, long receiveUserPtr, long posStart, long posEnd){
		
		int result = 0;
		
		//修正前后关系，避免SQL修改状态无效
		if(posEnd > posStart){
			
			long temp = -1;
			
			temp = posEnd;
			
			posEnd = posStart;
			
			posStart = temp;
			
		}
		
		try{
			
			final StringBuilder queryStr = new StringBuilder();

			queryStr.append(" UPDATE USER_TALK_INFO SET IS_READ_FLG='0', READ_UPDATE=SYSDATE() ");
			queryStr.append(" WHERE SEND_USER_PTR='" + sendUserPtr + "' AND RECEIVE_USER_PTR='" + receiveUserPtr + "' ");
			queryStr.append("     AND (PKEY <= '" + posStart + "' AND PKEY >= '" + posEnd + "' ) ");
			
			result = (int) this.hibernateTemplate.execute(new HibernateCallback<Object>() {
		    	
		    	public Object doInHibernate(final Session session)throws HibernateException, SQLException {
		    		
		    		Query query = session.createSQLQuery(queryStr.toString());
		    		
		    		return query.executeUpdate();
		    	}
		    });
    
		} catch (Exception e) {
			
			e.printStackTrace();
	   	}
		
		return result;
	}
	
	//获取聊天记录
	@SuppressWarnings("unchecked")
	public List<UserTalkInfoVO> getTalksList(long sendUserPtr, long receiveUserPtr, long pageSize){
		
		List<UserTalkInfoVO> list = new ArrayList<UserTalkInfoVO>();
		
		try {
			
			final StringBuilder queryStr = new StringBuilder();
						
			queryStr.append(" SELECT ");
			queryStr.append(" 	TALK.PKEY, ");
			queryStr.append(" 	TALK.SEND_USER_PTR, ");
			queryStr.append(" 	USER1.USER_NAME AS SEND_USER_NAME, ");
			queryStr.append(" 	V1.COVER_IMAGE_URL AS SEND_USER_IMAGE_URL, ");
			queryStr.append(" 	TALK.RECEIVE_USER_PTR, ");
			queryStr.append(" 	USER2.USER_NAME AS RECEIVE_USER_NAME, ");
			queryStr.append(" 	V2.COVER_IMAGE_URL AS RECEIVE_USER_IMAGE_URL, ");
			queryStr.append(" 	TALK.TALK_ABOUT, ");
			queryStr.append(" 	TALK.IS_READ_FLG, ");
			queryStr.append(" 	TALK.READ_UPDATE, ");
			queryStr.append(" 	(CASE WHEN TALK.IS_READ_FLG='1' THEN 'T' ELSE 'F' END) AS READ_CSS_NAME, ");
			queryStr.append(" 	(CASE WHEN TALK.SEND_USER_PTR='" + sendUserPtr + "' THEN 'LEFT' ELSE 'RIGHT' END) AS SELF_CSS_NAME, ");
			queryStr.append(" 	DATE_FORMAT(TALK.LAST_UPDATE, '%Y-%m-%d %h:%i') AS SEND_DATE, ");
			queryStr.append(" 	TALK.LAST_UPDATE, ");
			queryStr.append(" 	TALK.LAST_USER_PTR ");
			queryStr.append(" FROM USER_TALK_INFO TALK ");
			queryStr.append(" LEFT JOIN USER_INFO USER1 ON TALK.SEND_USER_PTR=USER1.PKEY ");
			queryStr.append(" LEFT JOIN COVER_IMAGE_URL_VIEW V1 ON V1.USER_PTR=USER1.PKEY ");
			queryStr.append(" LEFT JOIN USER_INFO USER2 ON TALK.RECEIVE_USER_PTR=USER2.PKEY ");
			queryStr.append(" LEFT JOIN COVER_IMAGE_URL_VIEW V2 ON V2.USER_PTR=USER2.PKEY ");
			queryStr.append(" WHERE ((TALK.SEND_USER_PTR='" + sendUserPtr + "' AND TALK.RECEIVE_USER_PTR='" + receiveUserPtr + "' AND TALK.IS_READ_FLG='0' ) ");
			queryStr.append(" 	OR (TALK.SEND_USER_PTR='" + receiveUserPtr + "' AND TALK.RECEIVE_USER_PTR='" + sendUserPtr + "')) ");			
			queryStr.append(" ORDER BY TALK.LAST_UPDATE DESC "); //TALK.pkey
			
			if(pageSize > 0){//
				
				queryStr.append(" limit " + pageSize);
			}
			
		    list = (List<UserTalkInfoVO>) this.hibernateTemplate.execute(new HibernateCallback<List<UserTalkInfoVO>>() {
		    	
		    	public List<UserTalkInfoVO> doInHibernate(final Session session)throws HibernateException, SQLException {
		    		
		    		Query query = session.createSQLQuery(queryStr.toString()).addEntity(UserTalkInfoVO.class);
		    		
		    		return query.list();
		    	}
		    });

		} catch (Exception e) {
			
			e.printStackTrace();
	   	}
		
		return list;		
	}
	
	//获取尚没有阅读过的聊天记录
	@SuppressWarnings("unchecked")
	public List<UserTalkInfoVO> getNewTalksList(long sendUserPtr, long receiveUserPtr, long pageSize){
		
		List<UserTalkInfoVO> list = new ArrayList<UserTalkInfoVO>();
		
		try {
			
			final StringBuilder queryStr = new StringBuilder();
						
			queryStr.append(" SELECT ");
			queryStr.append(" 	TALK.PKEY, ");
			queryStr.append(" 	TALK.SEND_USER_PTR, ");
			queryStr.append(" 	USER1.USER_NAME AS SEND_USER_NAME, ");
			queryStr.append(" 	V1.COVER_IMAGE_URL AS SEND_USER_IMAGE_URL, ");
			queryStr.append(" 	TALK.RECEIVE_USER_PTR, ");
			queryStr.append(" 	USER2.USER_NAME AS RECEIVE_USER_NAME, ");
			queryStr.append(" 	V2.COVER_IMAGE_URL AS RECEIVE_USER_IMAGE_URL, ");
			queryStr.append(" 	TALK.TALK_ABOUT, ");
			queryStr.append(" 	TALK.IS_READ_FLG, ");
			queryStr.append(" 	TALK.READ_UPDATE, ");
			queryStr.append(" 	(CASE WHEN TALK.IS_READ_FLG='1' THEN 'T' ELSE 'F' END) AS READ_CSS_NAME, ");
			queryStr.append(" 	(CASE WHEN TALK.SEND_USER_PTR='" + sendUserPtr + "' THEN 'LEFT' ELSE 'RIGHT' END) AS SELF_CSS_NAME, ");
			queryStr.append(" 	DATE_FORMAT(TALK.LAST_UPDATE, '%Y-%m-%d %h:%i') AS SEND_DATE, ");
			queryStr.append(" 	TALK.LAST_UPDATE, ");
			queryStr.append(" 	TALK.LAST_USER_PTR ");
			queryStr.append(" FROM USER_TALK_INFO TALK ");
			queryStr.append(" LEFT JOIN USER_INFO USER1 ON TALK.SEND_USER_PTR=USER1.PKEY ");
			queryStr.append(" LEFT JOIN COVER_IMAGE_URL_VIEW V1 ON V1.USER_PTR=USER1.PKEY ");
			queryStr.append(" LEFT JOIN USER_INFO USER2 ON TALK.RECEIVE_USER_PTR=USER2.PKEY ");
			queryStr.append(" LEFT JOIN COVER_IMAGE_URL_VIEW V2 ON V2.USER_PTR=USER2.PKEY ");
			queryStr.append(" WHERE (TALK.RECEIVE_USER_PTR='" + receiveUserPtr + "') ");
			
			//否则获取所有未读取的聊天信息
			if(sendUserPtr > 0){
				
				queryStr.append(" AND (TALK.SEND_USER_PTR='" + sendUserPtr + "') ");
			}					
					
			queryStr.append(" AND (TALK.IS_READ_FLG='1') ");
			queryStr.append(" ORDER BY TALK.LAST_UPDATE ASC "); //TALK.pkey
			
			if(pageSize > 0){//
				
				queryStr.append(" limit " + pageSize);
			}
			
		    list = (List<UserTalkInfoVO>) this.hibernateTemplate.execute(new HibernateCallback<List<UserTalkInfoVO>>() {
		    	
		    	public List<UserTalkInfoVO> doInHibernate(final Session session)throws HibernateException, SQLException {
		    		
		    		Query query = session.createSQLQuery(queryStr.toString()).addEntity(UserTalkInfoVO.class);
		    		
		    		return query.list();
		    	}
		    });

		} catch (Exception e) {
			
			e.printStackTrace();
	   	}
		
		return list;		
	}
	
}
