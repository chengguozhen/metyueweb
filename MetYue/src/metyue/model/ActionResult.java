package metyue.model;

import java.io.Serializable;

public class ActionResult implements Serializable{
	
	private static final long serialVersionUID = 2118464979598395479L;
	private String code;
	private String result;
	private String context;
	
	public ActionResult() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ActionResult(String code, String result, String context) {
		super();
		this.code = code;
		this.result = result;
		this.context = context;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}
	
	
}
