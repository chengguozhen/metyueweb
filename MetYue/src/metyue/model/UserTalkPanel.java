package metyue.model;

public class UserTalkPanel implements java.io.Serializable{
	
	private static final long serialVersionUID = 4280814879345741937L;
	
	private long sendUserPtr;
	private String sendUserName;
	private long receiveUserPtr;
	private int count;
	private String countMessage;
	private String style;
	
	public UserTalkPanel() {
		super();
	}

	public UserTalkPanel(long sendUserPtr, String sendUserName,
			long receiveUserPtr, int count, String countMessage, String style) {
		super();
		this.sendUserPtr = sendUserPtr;
		this.sendUserName = sendUserName;
		this.receiveUserPtr = receiveUserPtr;
		this.count = count;
		this.countMessage = countMessage;
		this.style = style;
	}

	public long getSendUserPtr() {
		return sendUserPtr;
	}

	public void setSendUserPtr(long sendUserPtr) {
		this.sendUserPtr = sendUserPtr;
	}

	public String getSendUserName() {
		return sendUserName;
	}

	public void setSendUserName(String sendUserName) {
		this.sendUserName = sendUserName;
	}

	public long getReceiveUserPtr() {
		return receiveUserPtr;
	}

	public void setReceiveUserPtr(long receiveUserPtr) {
		this.receiveUserPtr = receiveUserPtr;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getCountMessage() {
		return countMessage;
	}

	public void setCountMessage(String countMessage) {
		this.countMessage = countMessage;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	
	
	
}
