package metyue.action;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import metyue.model.ActionResult;
import metyue.model.RespUserTalkInfoVO;
import metyue.model.UserOnlineStatus;
import metyue.model.UserSessionInfo;
import metyue.model.UserTalkInfo;
import metyue.model.UserTalkInfoVO;
import metyue.model.UserTalkPanel;
import metyue.service.ActionResultService;
import metyue.service.ImprintService;
import metyue.service.UserTalkService;
import metyue.utils.WebUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.WebUtils;

@Controller
@RequestMapping(value = "/talk")
public class UserTalkAction {

	@Autowired
	private UserTalkService userTalkService;
	
	@Autowired
	private ActionResultService actionResultService;
	
	@Autowired
	private ImprintService imprintService;	
	
	//get talk tips info
	@RequestMapping(value = "/getUserNewTalks")
	public void getUserNewTalks(HttpServletRequest request, HttpServletResponse response){
		
		ActionResult objResult = null;
		
		String talker = WebUtils.findParameterValue(request, "Talker");
		
		try{
			
			UserSessionInfo session = (UserSessionInfo)WebUtil.getUserSession(request, "UserInfo");
			
			if(session != null){
				
				long userPtr = (long)session.getUserPtr();
				
				//get talk info
				List<UserTalkInfoVO> lst = userTalkService.news(Long.parseLong(talker), userPtr, 100L);
				
				//update talk info read-flag
				if((lst != null) && (lst.size() > 0)) {
					
					long posBegin = lst.get(0).getPkey();
					
					long posEnd = lst.get(lst.size()-1).getPkey();
				
					userTalkService.rest(Long.parseLong(talker), userPtr, posBegin, posEnd);
				}
				
				//print json
				WebUtil.responseToJson( response, lst );  //AAA
				
			}else{
				
				objResult = actionResultService.getActionResultItem("20008");
				
				WebUtil.responseToJson( response, objResult );  //BBB
				
			}
			
		}catch (Exception e) {
			
			objResult = actionResultService.getActionResultItem("10000");
			
			e.printStackTrace();
			
			WebUtil.responseToJson( response, objResult );
			
		}
	}
	
	//get talk tips info
	@RequestMapping(value = "/getUserTalkTips")
	public void getUserTalkTips(HttpServletRequest request, HttpServletResponse response){
		
		ActionResult objResult = null;
		
		try{
			
			UserSessionInfo session = (UserSessionInfo)WebUtil.getUserSession(request, "UserInfo");
			
			if(session != null){
				
				long userPtr = (long)session.getUserPtr();
				
				int tipsCount = userTalkService.getNotReadTalkInfoCount(userPtr);
				
				WebUtil.responseToJson( response,  actionResultService.getActionResultItem("10007", String.valueOf(tipsCount)) );
				
			}
			
		}catch (Exception e) {
			
			objResult = actionResultService.getActionResultItem("10000");
			
			e.printStackTrace();
			
			WebUtil.responseToJson( response, objResult );
			
		}
	}
	
	//get talk list info
	@RequestMapping(value = "/getTalkAboutInfo")
	public void getTalkAboutInfo(HttpServletRequest request, HttpServletResponse response){
		
		ActionResult objResult = null;
		
		String talker = WebUtils.findParameterValue(request, "Talker");
		
		try{
			
			UserSessionInfo session = (UserSessionInfo)WebUtil.getUserSession(request, "UserInfo");
			
			if(session != null){
				
				long userPtr = (long)session.getUserPtr();
				
				//get talk info
				List<UserTalkInfoVO> lst = userTalkService.read(Long.parseLong(talker), userPtr, 100L);
				
				//print json
				WebUtil.responseToJson( response, lst );  //AAA
				
			}else{
				
				objResult = actionResultService.getActionResultItem("20008");
				
				WebUtil.responseToJson( response, objResult );  //BBB
				
			}
			
		}catch (Exception e) {
			
			objResult = actionResultService.getActionResultItem("10000");
			
			e.printStackTrace();
			
			WebUtil.responseToJson( response, objResult );  //CCC
			
		}
		
	}
	
	//get talk pannel info
	@RequestMapping(value = "/getTalkPannelInfo")
	public void getTalkPannelInfo(HttpServletRequest request, HttpServletResponse response){

		ActionResult objResult = null;
		
		try{
			
			UserSessionInfo session = (UserSessionInfo)WebUtil.getUserSession(request, "UserInfo");
			
			if(session != null){
				
				long userPtr = (long)session.getUserPtr();
				
				List<UserTalkPanel> lst = userTalkService.getUserTalkPanelList(userPtr);
				
				WebUtil.responseToJson( response, lst ); //AAA
				
				
			}else{
				
				objResult = actionResultService.getActionResultItem("20008");
				
				WebUtil.responseToJson( response, objResult );  //BBB
				
			}
			
		}catch (Exception e) {
			
			objResult = actionResultService.getActionResultItem("10000");
			
			e.printStackTrace();
			
			WebUtil.responseToJson( response, objResult );  //CCC
			
		}
		
	}
	
	//send message
	@RequestMapping(value = "/sendMessage")
	public void sendMessage(HttpServletRequest request, HttpServletResponse response){
		
		ActionResult objResult = null;
		
		String recevieUserPtr = WebUtils.findParameterValue(request, "RecevieUserPtr");
		String talkAbout = WebUtil.decode2( WebUtils.findParameterValue(request, "TalkAbout") );
		
		String appSource = WebUtil.decode2( WebUtils.findParameterValue(request, "AppSource") );
		
		try{				
			
			UserSessionInfo session = (UserSessionInfo)WebUtil.getUserSession(request, "UserInfo");
			
			if(session != null){	

				if("".equals(recevieUserPtr)){
					
					objResult = actionResultService.getActionResultItem("10001");
					
				}else{
					
					long userPtr = (long)session.getUserPtr();					
					
					//构建信息实体
					UserTalkInfo userTalkInfo = new UserTalkInfo();
					userTalkInfo.setSendUserPtr(userPtr);
					userTalkInfo.setReceiveUserPtr(Long.parseLong(recevieUserPtr));
					userTalkInfo.setTalkAbout(talkAbout);
					userTalkInfo.setLastUserPtr(userPtr);
					userTalkInfo.setReadFlg("1");
					userTalkInfo.setLastUpdate(new Date());
					userTalkInfo.setAppSource(appSource);					
					
					//天啊，趁热打铁开始
					userTalkService.send(userTalkInfo);
					
					//保存在线时间，不记录聊天日志
					UserOnlineStatus userOnlineStatus = imprintService.getUserOnlineStatus(userPtr);
					userOnlineStatus.setLastOptDate(new Date());
					imprintService.saveUserOnlineStatus(userOnlineStatus);
					
					objResult = actionResultService.getActionResultItem("20023");
					
				}

			}else{
				
				objResult = actionResultService.getActionResultItem("20008");
				
			}
			
		}catch (Exception e) {
			
			objResult = actionResultService.getActionResultItem("10000");
			
			e.printStackTrace();
			
		}
		
		WebUtil.responseToJson( response, objResult ); 
		
	}
	
}


